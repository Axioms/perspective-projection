
/**
 * Projecting 3D points onto a 2D plane.
 */

#include <iostream>
#include <cmath>
#include <array>
#include <SFML/Graphics.hpp>

const int SCREEN_WIDTH = 1400;
const int SCREEN_HEIGHT = 1400;
const int NUM_POINTS = 10000;
const float SPEED = 5;

struct Vector3 {
    float x;
    float y;
    float z;

    //Construct vector
    Vector3(float x0 = 0, float y0 = 0, float z0 = 0) :
            x(x0),
            y(y0),
            z(z0) {
    }

    //Magnitude of vector
    float magnitude() {
        return sqrt(x * x + y * y + z * z);
    }

    //Dot product with another vector
    float dot(Vector3 other) {
        return x * other.x + y * other.y + z * other.z;
    }

    //Cross product with another vector (<this> x <other>)
    Vector3 cross(Vector3 other) {
        return Vector3(y * other.z - z * other.y, -(x * other.z - z * other.x),
                x * other.y - y * other.x);
    }
};

std::array<bool, sf::Keyboard::KeyCount> keyStates;

bool isKeyPressed(sf::Keyboard::Key key) {
    return keyStates[static_cast<int>(key)];
}

int main() {
    /////////// Window and points setup ////////////

    //Set-up the window
    sf::VideoMode videoMode(SCREEN_WIDTH, SCREEN_HEIGHT);
    sf::RenderWindow renderWindow(videoMode, "Perspective Projection");

    keyStates.fill(false);

    //Set-up points
    srand(time(NULL));
    Vector3 points[NUM_POINTS];
    for (int i = 0; i < NUM_POINTS; i += 1) {
        points[i].x = rand() % SCREEN_WIDTH;
        points[i].y = rand() % SCREEN_WIDTH;
        points[i].z = rand() % SCREEN_HEIGHT;

//        points[i + 1].x = points[i].x;
//        points[i + 1].y = points[i].y + 10;
//        points[i + 1].z = points[i].z;
    }

    //Make box around points
    Vector3 box[24];
    box[0] = Vector3(0, 0, 0);
    box[1] = Vector3(0, SCREEN_WIDTH, 0);
    box[2] = Vector3(0, SCREEN_WIDTH, 0);
    box[3] = Vector3(SCREEN_WIDTH, SCREEN_WIDTH, 0);
    box[4] = Vector3(SCREEN_WIDTH, SCREEN_WIDTH, 0);
    box[5] = Vector3(SCREEN_WIDTH, 0, 0);
    box[6] = Vector3(SCREEN_WIDTH, 0, 0);
    box[7] = Vector3(0, 0, 0);

    box[8] = Vector3(0, 0, SCREEN_HEIGHT);
    box[9] = Vector3(0, SCREEN_WIDTH, SCREEN_HEIGHT);
    box[10] = Vector3(0, SCREEN_WIDTH, SCREEN_HEIGHT);
    box[11] = Vector3(SCREEN_WIDTH, SCREEN_WIDTH, SCREEN_HEIGHT);
    box[12] = Vector3(SCREEN_WIDTH, SCREEN_WIDTH, SCREEN_HEIGHT);
    box[13] = Vector3(SCREEN_WIDTH, 0, SCREEN_HEIGHT);
    box[14] = Vector3(SCREEN_WIDTH, 0, SCREEN_HEIGHT);
    box[15] = Vector3(0, 0, SCREEN_HEIGHT);

    box[16] = Vector3(0, 0, 0);
    box[17] = Vector3(0, 0, SCREEN_HEIGHT);
    box[18] = Vector3(0, SCREEN_WIDTH, 0);
    box[19] = Vector3(0, SCREEN_WIDTH, SCREEN_HEIGHT);
    box[20] = Vector3(SCREEN_WIDTH, SCREEN_WIDTH, 0);
    box[21] = Vector3(SCREEN_WIDTH, SCREEN_WIDTH, SCREEN_HEIGHT);
    box[22] = Vector3(SCREEN_WIDTH, 0, 0);
    box[23] = Vector3(SCREEN_WIDTH, 0, SCREEN_HEIGHT);

    sf::VertexArray boxVertices(sf::PrimitiveType::Lines, 24);
    sf::VertexArray vertices(sf::PrimitiveType::Points, NUM_POINTS);

    Vector3 screenHorizontal(SCREEN_WIDTH, 0, 0);
    Vector3 screenVertical(0, 0, SCREEN_HEIGHT);
    Vector3 camera(SCREEN_WIDTH / 2, -50, SCREEN_HEIGHT / 2);
    Vector3 camPos(SCREEN_WIDTH / 2, -50, SCREEN_HEIGHT / 2);

    /////////// Main program loop ////////////
    while (renderWindow.isOpen()) {

        /////////// Handle input events ////////////
        sf::Event event;
        while (renderWindow.pollEvent(event)) {
            switch (event.type) {
                case (sf::Event::Closed):
                    renderWindow.close();
                    break;
                case (sf::Event::KeyPressed):
                    keyStates[static_cast<int>(event.key.code)] = true;
                    if (event.key.code == sf::Keyboard::Escape) {
                        renderWindow.close();
                    }
                    break;
                case (sf::Event::KeyReleased):
                    keyStates[static_cast<int>(event.key.code)] = false;
                    break;
                default:
                    break;
            }
        }
        if (isKeyPressed(sf::Keyboard::Left)) {
            camPos.x -= SPEED;
        }
        if (isKeyPressed(sf::Keyboard::Right)) {
            camPos.x += SPEED;
        }
        if (isKeyPressed(sf::Keyboard::Up)) {
            camPos.z -= SPEED;
        }
        if (isKeyPressed(sf::Keyboard::Down)) {
            camPos.z += SPEED;
        }
        if (isKeyPressed(sf::Keyboard::W)) {
            camPos.y += SPEED/2;
        }
        if (isKeyPressed(sf::Keyboard::S)) {
            camPos.y -= SPEED/2;
        }

        Vector3 normal = screenHorizontal.cross(screenVertical);
        //Update 2D projections of box
        for (int i = 0; i < boxVertices.getVertexCount(); i++) {
            sf::Vector2f pos;

            float t = (normal.z * screenHorizontal.z - normal.z * camera.z
                    + normal.y * screenHorizontal.y - normal.y * camera.y
                    + normal.x * screenHorizontal.x - normal.x * camera.x)
                    / (normal.x * (box[i].x - camPos.x)
                            + normal.y * (box[i].y - camPos.y)
                            + normal.z * (box[i].z - camPos.z));

            pos.x = camera.x + (box[i].x - camPos.x) * t;
            pos.y = camera.z + (box[i].z - camPos.z) * t;

            if (box[i].y < camPos.y) {
                pos.x = 0;
                pos.y = 0;
            }
            boxVertices[i] = sf::Vertex(pos, sf::Color::Black);
        }

        //Update 2D projections of points
        for (int i = 0; i < vertices.getVertexCount(); i++) {
            sf::Vector2f pos;

            float t = (normal.z * screenHorizontal.z - normal.z * camera.z
                    + normal.y * screenHorizontal.y - normal.y * camera.y
                    + normal.x * screenHorizontal.x - normal.x * camera.x)
                    / (normal.x * (points[i].x - camPos.x)
                            + normal.y * (points[i].y - camPos.y)
                            + normal.z * (points[i].z - camPos.z));

            pos.x = camera.x + (points[i].x - camPos.x) * t;
            pos.y = camera.z + (points[i].z - camPos.z) * t;

            if (points[i].y < camPos.y) {
                pos.x = 0;
                pos.y = 0;
            }
            vertices[i] = sf::Vertex(pos, sf::Color::Black);
        }

        /////////// Update window display ////////////
        renderWindow.clear(sf::Color::White);

        renderWindow.draw(vertices);
        renderWindow.draw(boxVertices);

        renderWindow.display();
    }
}

