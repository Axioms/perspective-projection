# Perspective-Projection
Experimenting with vectors and testing how to project 3D points onto a digital 2D camera plane.  
[View Source Code](https://bitbucket.org/Axioms/perspective-projection/src)

## Example
![Example](http://i.imgur.com/hn20bbn.gif)

[(Direct Link (.gif))](http://i.imgur.com/hn20bbn.gifv)

## Project Reflection
A natural drawback of any screen is that what it shows can only be displayed in two dimensions. In order to display three dimensional imagery, the objects must be projected onto the 2D plane of the screen. This program was created with the intent of experimenting how this can be achieved using only vectors.

![Image Plane Diagram](http://www.povray.org/documentation/images/reference/perspcam.png)  

[(Direct Link (.png))](http://www.povray.org/documentation/images/reference/perspcam.png)  

The above diagram illustrates the basic method of projecting a 3D point onto a 2D plane. It consists of three primary components. First is the target object, here a single point in 3D space. The second is the 2D plane representing the screen onto which this point will be displayed. The final component is an observer, positioned on the opposite side of the plane from the object being drawn. 

With this setup, the procedure is very simple, and can be broken down into the following steps:  

1. **A line between the observer point (O) and the target point (T) being drawn onto the screen is first created. This line is defined in parametric form as:**  
  x = x0 + at  
  y = y0 + bt  
  z = z0 + ct  
  Where **v=<a,b,c>** is the vector parallel to the line and **r0=<x0,y0,z0>** is a position vector for any point on the line. Either the observer or target points can serve equally well as as **r0**, since they by definition both lie on the line. The parallel vector be obtained by simply subtracting one of the points from the other, e.g. **O-T = <Xo - Xt, Yo - Yt, Zo - Zt> = v**.

2. **The equation for the camera plane in parametric form is devised:**  
  a(x - x0) + b(y - y0) + c(z - z0) = 0  
  Where **n=<a,b,c>** is a normal vector to the plane and **r0=<x0,y0,z0>** is a position vector for any vector parallel to the plane. For this demo program, the plane was defined by two orthogonal vectors, one aligned with the X axis, and the other with the Z. To obtain the normal vector **n**, the cross product of these two was calculated, yielding a vector perpendicular to both, and thus perpendicular to the plane. 

3. **The intersection between this line and plane is determined.**  
  By substituting the parametric equations for the line into the equation for the plane, the variable t can be isolated and solved for:  
  a1(x - x0) + b1(y - y0) + c1(z - z0) = 0  
  a1(x0 + a2t - x0) + b1(y0 + b2t - y0) + c1(z0 + c2t - z0) = 0  
  a1x0 + a1a2t - a1x0 + b1y0 + b1b2t - b1y0 + c1z0 + c1c2t - c1z0 = 0  
  a1a2t + b1b2t + c1c2t = c1z0 - c1z0 + b1y0 - b1y0 + a1x0 - a1x0  
  t(a1a2 + b1b2 + c1c2) = c1z0 - c1z0 + b1y0 - b1y0 + a1x0 - a1x0  
  **t = (c1z0 - c1z0 + b1y0 - b1y0 + a1x0 - a1x0) / (a1a2 + b1b2 + c1c2)**  
  This value can then be put into the equation for the line, yielding the coordinates of the intersection point, **I**. This point lies on plane, and thus, the screen.

4. **Because the final representation will be in 2D, one dimension will need to be ignored: Y, which in this setup represents depth. Thus, final 2D coordinates of the point on the screen as given as such:**  
  X = **I**x  
  Y = **I**z  
  These points can now be drawn onto the screen while accurately representing the position of a point in 3D space.

## Further Reflection
While this method of 3D to 2D visualization is sufficient in simple scenarios, it does have drawbacks. For example, if the point being drawn is behind the observer, then the line between it and the observer point will "flip" and intersect the screen plane on the opposite side (t in the parametric equation will be negative for the intersection solution), creating a mirror image when nothing should appear at all. While the simple solution of just not drawing any points that are offscreen might work if this point is all alone, it fails if the point is only one of many connected vertices in a complicated object, as onscreen geometry would disappear, proving this problem to be an increasingly complicated one.

## Additional Details
This program was created using the [SFML](http://www.sfml-dev.org/) graphics library.